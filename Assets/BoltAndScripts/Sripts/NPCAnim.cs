using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCAnim : MonoBehaviour
{

    NavMeshAgent nav;
    Animator animPlayer;
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        animPlayer = GetComponent<Animator>();
    }

    void Update()
    {
        if (nav.destination == transform.position)
        {
            nav.isStopped = true;
            animPlayer.SetBool("Moving", false);
        }
        else
        {
            nav.isStopped = false;
            animPlayer.SetBool("Moving", true);
        }
    }
}