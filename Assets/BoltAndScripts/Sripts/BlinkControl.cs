using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ludiq;
using Bolt;

public class BlinkControl : MonoBehaviour
{
    public float Amount;
    public Material Mat;
    [ColorUsageAttribute(true, true)]
    public Color RCol;
    [ColorUsageAttribute(true, true)]
    public Color WCol;
    public float BlinkSpeed;
    public bool done = false;
    public bool none = false;
    public GameObject Player;

    // Update is called once per frame
    void Update()
    {
        Mat = gameObject.GetComponentInChildren<Renderer>().material;
        Player = (GameObject)Variables.ActiveScene.Get("ActiveObject");
        done = (bool)Variables.Object(Player).Get("FlagDone");
        none = (bool)Variables.Object(Player).Get("FlagNone");
        if (done)
        {
            Mat.SetColor("_Blink", WCol);
            Amount = Mathf.Lerp(Amount, 0, Time.deltaTime * BlinkSpeed);
            Amount = Mathf.Clamp(Amount, 0, 1);
            if (Amount < 0.1)
            {
                Amount += 1;
            }
            Mat.SetFloat("_Amount", Amount);
        }
        else if (none)
        {
            Mat.SetColor("_Blink", RCol);
            Amount = Mathf.Lerp(Amount, 0, Time.deltaTime * BlinkSpeed);
            Amount = Mathf.Clamp(Amount, 0, 1);
            if (Amount < 0.1)
            {
                Amount += 1;
            }
            Mat.SetFloat("_Amount", Amount);
        }
        else Mat.SetFloat("_Amount", 0);
    }
}
