using System.Numerics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ludiq;
using Bolt;

public class FindPath : MonoBehaviour
{
    public int StartPointX;
    public int StartPointY;
    public int EndX;
    public int EndY;
    public int[,] mas = { {1, 1, 1, 1, 1}, 
                          {1, 0, 1, 1, 1}, 
                          {1, 1, 1, 0, 1}, 
                          {1, 1, 0, 1, 1}, 
                          {1, 1, 1, 1, 1} };
    string cl = "";
    public int COST = 25;

    private void OnMouseDown()
    {
        //NESW[0] = 6;
        StartPointX = (int)Variables.ActiveScene.Get("StX");
        StartPointY = (int)Variables.ActiveScene.Get("StY");
        if(StartPointX != EndX || StartPointY != EndY)
        {
            Variables.ActiveScene.Set("Move", true);
            find(StartPointX, StartPointY, cl, 0);
            Variables.ActiveScene.Set("StX", EndX);
            Variables.ActiveScene.Set("StY", EndY);
            COST = 25;
        }
        else
        {
            Variables.ActiveScene.Set("Move", false);
        }
        //Debug.Log(NESW[0]);
        //Debug.Log("GG");
    }

    public void find(int X, int Y, string S, int cost)
    {
        //Debug.Log(nesw[0]);
        if(cost < COST)
        {
            if(Y-1 >= 0)
            {
                // int[] left = nesw;
                // left[cost] = 4;
                if (mas[X, Y-1] == 1)
                {
                    //nesw[cost] = 4;
                    if(X == EndX && (Y - 1) == EndY)
                    {
                        if(cost+1<COST)
                        {
                            Variables.ActiveScene.Set("Nav", null);
                            Variables.ActiveScene.Set("Nav", S + "4");
                            COST = cost + 1;
                            //Debug.Log(cost);
                        }
                    }
                    else
                    {
                        find(X, Y-1, S + "4", cost + 1);
                    }
                }
            }

            if(Y+1 <= 4)
            {
                // int[] right = nesw;
                // right[cost] = 2;
                if (mas[X, Y+1] == 1)
                {
                    //nesw[cost] = 2;
                    if(X == EndX && (Y + 1) == EndY)
                    {
                        if(cost+1<COST)
                        {
                            Variables.ActiveScene.Set("Nav", null);
                            Variables.ActiveScene.Set("Nav", S + "2");
                            COST = cost + 1;
                        }
                    }
                    else
                    {
                        find(X, Y+1, S + "2", cost + 1);
                    }
                }
            }

            if(X-1 >= 0)
            {
                // int[] up = nesw;
                // up[cost] = 1;
                if (mas[X-1, Y] == 1)
                {
                    //nesw[cost] = 1;
                    if((X - 1) == EndX && Y == EndY)
                    {
                        if(cost+1<COST)
                        {
                            Variables.ActiveScene.Set("Nav", null);
                            Variables.ActiveScene.Set("Nav", S + "1");
                            COST = cost + 1;
                        }
                    }
                    else
                    {
                        find(X-1, Y, S + "1", cost + 1);
                    }
                }
            }

            if(X+1 <=4)
            {
                // int[] down = nesw;
                // down[cost] = 3;
                if (mas[X+1, Y] == 1)
                {
                    //nesw[cost] = 3;
                    if((X + 1) == EndX && Y == EndY)
                    {
                        if(cost+1<COST)
                        {
                            Variables.ActiveScene.Set("Nav", null);
                            Variables.ActiveScene.Set("Nav", S + "3");
                            COST = cost + 1;
                        }
                    }
                    else
                    {
                        find(X+1, Y, S + "3", cost + 1);
                    }
                }
            }
        }
    }
}

